import argparse
import logging
import mimetypes
import os
import smtplib
import sys
import types

from email.message import EmailMessage

class Config(dict):

    def from_object(self, obj):
        for key in dir(obj):
            if key.isupper():
                self[key] = getattr(obj, key)

    def from_pyfile(self, path):
        d = types.ModuleType('config')
        d.__file__ = path
        with open(path, mode='rb') as config_file:
            exec(compile(config_file.read(), path, 'exec'), d.__dict__)
        self.from_object(d)
        return True


def main(argv=None):
    """
    CLI Python app to send an email using SMTP.
    """
    # This little project was started because a file was going to be emailed to
    # several people but I haven't heard about it for over a week.
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument('to', nargs='+')
    parser.add_argument('-a', '--attach', nargs='+')
    parser.add_argument('-b', '--body')
    parser.add_argument('-f', '--from', dest='from_', required=True)
    parser.add_argument('-H', '--host', default='')
    parser.add_argument('-P', '--port', type=int)
    parser.add_argument('-s', '--subject', default='')
    args = parser.parse_args(argv)

    with smtplib.SMTP(args.host, args.port) as smtp:
        email = EmailMessage()
        email['From'] = args.from_
        email['To'] = ', '.join(args.to)
        email['Subject'] = args.subject
        if args.body:
            email.set_content(args.body)
        for path in args.attach:
            continue
            ctype, encoding = mimetypes.guess_type(path)
            if ctype is None or encoding is not None:
                ctype = 'application/octet-stream'
            maintype, subtype = ctype.split('/', 1)
            filename = os.path.basename(path)
            with open(path, 'rb') as fp:
                email.add_attachment(fp.read(), maintype, subtype, filename)

        refused = smtp.send_message(email)
        if refused:
            print(f'refused: {refused}', file=sys.stderr)

if __name__ == '__main__':
    main()
